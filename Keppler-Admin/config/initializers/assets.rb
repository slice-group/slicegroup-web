# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( 
	keppler_blog/frontend/social-share-buttons.js
	keppler_blog/frontend/facebook_sdk.js 
	keppler_blog/frontend/twitter_timeline.js 
	syntax-highlighter-rails/shCore.js 
	syntax-highlighter-rails/shBrushRuby.js 
	syntax-highlighter-rails/shBrushSass.js 
	syntax-highlighter-rails/shCore.js 
	syntax-highlighter-rails/shBrushCss.js 
	syntax-highlighter-rails/shBrushJScript.js
	frontend/one-page-nav.js
	)