Rails.application.routes.draw do

  post 'newsletter/create', as: "newsletters"

  #blog
  mount KepplerBlog::Engine, :at => '/', as: 'blog'

  #catalog
  mount KepplerCatalogs::Engine, :at => '/', as: 'catalogs'

  scope "(:locale)", :locale => /en|es/ do
    root to: 'frontend#index'
    resources :admin, only: :index
    devise_for :users, skip: KepplerConfiguration.skip_module_devise

    scope :admin do
      resources :users do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      resources :type_projects do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
        resources :services do
          get '(page/:page)', action: :index, on: :collection, as: ''
          delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
        end
      end

      resources :planners, :except => [:new, :create] do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end
    end

    #protfolio
    get 'projects/:catalog_permalink', to: "frontend#projects", as: "projects"
    get 'portfolio/:section', to: "frontend#portfolio_section", as: "portfolio_section"

    #briefing form
    get 'briefing', to: 'planners#briefing', as: :breafing
    post 'briefing', to: 'planners#create'
    post 'briefing/services/:type_project_id', to: 'planners#planner_services_json'
    post 'briefing/type_project/:type_project_id', to: 'planners#planner_type_project_json'

    #errors
    match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
    match '/404', to: 'errors#not_found', via: :all
    match '/422', to: 'errors#unprocessable', via: :all
    match '/500', to: 'errors#internal_server_error', via: :all

    #dashboard
    mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'

    #contact_us
    mount KepplerContactUs::Engine, :at => '/', as: 'messages'
  end
end
