# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

#create roles for default
[:admin, :autor, :editor].each do |name|
	Role.create name: name
	puts "#{name} creado"
end

#create user for default
User.create name: "Admin", email: "admin@inyxtech.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
puts "admin@inyxtech.com ha sido creado"

#create catalog to carousel for default
KepplerCatalogs::Catalog.create(name: "Carrusel de partners", description: "Carrusel de partners", section: "partners", public: true)
KepplerCatalogs::Catalog.create(name: "Carrusel de clientes", description: "Carrusel de clientes", section: "clients", public: true)