class CreateCatalogTranslations < ActiveRecord::Migration
  def up
    KepplerCatalogs::Catalog.create_translation_table! name: :string, description: :text
  end

  def down
    KepplerCatalogs::Catalog.drop_translation_table!
  end
end
