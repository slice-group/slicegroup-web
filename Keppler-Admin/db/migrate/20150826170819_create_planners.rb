class CreatePlanners < ActiveRecord::Migration
  def change
    create_table :planners do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.date :date_begin
      t.date :date_end
      t.text :description
      t.string :budget
      t.integer :machines
      t.integer :persons
      t.belongs_to :type_project
      t.boolean :read, default: false

      t.timestamps null: false
    end

    create_table :planners_services, id: false do |t|
      t.belongs_to :planner, index: true
      t.belongs_to :service, index: true
    end
    
  end
end
