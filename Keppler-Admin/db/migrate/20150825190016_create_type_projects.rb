class CreateTypeProjects < ActiveRecord::Migration
  def change
    create_table :type_projects do |t|
      t.string :name
      t.text :description
      t.boolean :date_begin 
      t.boolean :date_end
      t.boolean :budget
      t.boolean :machines
      t.boolean :persons
      t.string :departament

      t.timestamps null: false
    end
  end
end
