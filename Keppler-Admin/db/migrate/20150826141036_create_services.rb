class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.belongs_to :type_project, index: true

      t.timestamps null: false
    end
    add_foreign_key :services, :type_projects
  end
end
