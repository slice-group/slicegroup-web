class CreateTypeProjectTranslations < ActiveRecord::Migration
  def up
    TypeProject.create_translation_table! name: :string, description: :text
  end

  def down
    TypeProject.drop_translation_table!
  end
end
