#Generado por keppler
require 'elasticsearch/model'
class TypeProject < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_many :services, :dependent => :destroy
  has_many :planners
  validates_presence_of :name, :departament
  translates :name, :description

  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name.to_s,
      description:  self.description.to_s,
      departament: self.departament
    }.as_json
  end

end
#TypeProject.import
