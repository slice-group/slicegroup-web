#Generado por keppler
require 'elasticsearch/model'
class Planner < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_and_belongs_to_many :services
  belongs_to :type_project  
  validates_presence_of :name, :email, :phone
  validate :validate_type_project
  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:name, :date_begin, :date_end, :type_project, :departament] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      email:  self.email,
      phone:  self.phone,
      date_begin:  self.date_begin,
      date_end:  self.date_end,
      description:  self.description,
      type_project: self.type_project.name,
      departament: self.type_project.departament,
      budget:  self.budget.to_s,
    }.as_json
  end

  def validate_services
    if self.service_ids.empty?
      errors.add(:services_error, "Debes seleccionar por lo menos un servicio.")
    end
  end

  def validate_type_project
    if self.type_project.nil?
      errors.add(:type_project_error, "Debes seleccionar el tipo de proyecto.")
    end
  end

end
#Planner.import
