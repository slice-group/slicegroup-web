removeHash = ->
	history.pushState '', document.title, window.location.pathname + window.location.search
	return

$(document).on 'ready page:load', () ->   
	#one-page-nav
	$('.top-nav').onePageNav
		currentClass: 'current'
		changeHash: false
		scrollSpeed: 500
		filter: ':not(.external)'
		easing: 'swing'
		begin: ->
			removeHash()
			return
		end: ->
			if $('.top-nav .current').find('a').attr('href') == '#about'
				$('.count').each ->
				  $(this).prop('Counter', 0).animate { Counter: $(this).text() },
				    duration: 3000
				    easing: 'swing'
				    step: (now) ->
				      $(this).text Math.ceil(now)
				      return
						$('.numbers').removeClass("count");
				  return
			if $('.top-nav .current').find('a').attr('href') != '#home'
				$('#front-navbar').removeClass 'background-hide'
				$('#front-navbar').addClass 'background-show'
			else
				$('#front-navbar').removeClass 'background-show'
				$('#front-navbar').addClass 'background-hide'

			return
		scrollChange: ($currentListItem) ->
			if $('.top-nav .current').find('a').attr('href') != '#home'
				$('#front-navbar').removeClass 'background-hide'
				$('#front-navbar').addClass 'background-show'
			else
				$('#front-navbar').removeClass 'background-show'
				$('#front-navbar').addClass 'background-hide'
				
			if $('.top-nav .current a').attr('href') == '#about'
				$('.count').each ->
				  $(this).prop('Counter', 0).animate { Counter: $(this).text() },
				    duration: 3000
				    easing: 'swing'
				    step: (now) ->
				      $(this).text Math.ceil(now)
				      return
				    $('.numbers').removeClass("count");
				  return
			return