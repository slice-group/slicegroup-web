angular.module('slicegroup', [
	'frontend'
]).config [
	'$httpProvider'
	(provider) ->
		# permite leer csrf token y añadirlo al ajax de angular para poder autenticar la seguridad de la aplicación
		provider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
		return
	]

$(document).on 'ready page:load', () ->   
	angular.bootstrap document.body, [ 'slicegroup' ] #añadir ng-app al body
	

	#scroll
	$(".scroll-down").on "click", ->
		aTag = $("#services")
		$("html,body").animate
			scrollTop: aTag.offset().top - 100
		, "slow"

	# Carousel
	$('.slick').slick
		slidesToShow: 4
		slidesToScroll: 1
		autoplay: true
		autoplaySpeed: 3000
		arrows: false
		lazyLoad: "ondemand"
		responsive: [
			{
				breakpoint: 1024
				settings:
					slidesToShow: 3
					slidesToScroll: 1
			}
			{
				breakpoint: 600
				settings:
					slidesToShow: 2
					slidesToScroll: 1
			}
			{
				breakpoint: 480
				settings:
					slidesToShow: 1
					slidesToScroll: 1
			}
		]


#out turbolinks
tabChange = ->
	tabs = $('.nav-tabs > li')
	active = tabs.filter('.active')
	next = if active.next('li').length then active.next('li').find('a') else tabs.filter(':first-child').find('a')
	# Use the Bootsrap tab show method
	next.tab 'show'
	return

# Tab Cycle function
tabCycle = setInterval(tabChange, 3000)

