$(document).ready(function(){
  var floatBtn = $(".floating-button");
  var floatTxt = $(".floating-text");
  var closeBtn = $(".floating-close");

  floatBtn.click(function(){
    floatBtn.removeAttr("id", "#float-btn");
    floatBtn.addClass("float-btn-expand");
    floatTxt.css("display", "flex");
    floatTxt.addClass("floating-text-show ");
  });

  closeBtn.click(function(){
    floatBtn.attr("id", "#float-btn");
    floatTxt.removeClass("floating-text-show ");
    floatTxt.css("display", "none");
    floatBtn.removeClass("float-btn-expand");
  });
});
