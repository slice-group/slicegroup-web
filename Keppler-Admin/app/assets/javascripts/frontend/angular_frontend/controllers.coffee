app.controller 'MainCtrl', [
	'$scope', "$compile", "$http", "$timeout", 'MainService'
	(scope, compile, http, timeout, MainService) ->

		scope.type_project = {}
		scope.data_type_project = {}
		scope.services = {}
		scope.budget = {}
		scope.service = {}

		scope.init = (locale, id)->
			scope.type_project.id = id
			MainService.getServices(scope, locale, id)
			MainService.getTypeProject(scope, locale, id)
			return

		scope.getServiceButtons = (locale, id)->
			scope.type_project.id = id
			MainService.getServices(scope, locale, id)
			MainService.getTypeProject(scope, locale, id)
			scope.service = {}
			return

		scope.selectBudget = (id)->
			scope.budget.id = id
			return

		scope.getDescriptionService = (id)->
			scope.service = {}
			scope.services.filter (element)->
				if element.id == id
					scope.service = {
						name: element.name,
						description: element.description
					}
					return
				return
			return

		return

]

