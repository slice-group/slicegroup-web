app.service 'MainService', [
	'$http'
	'localStorageService'
	(http, localStorage)->

		this.url = window.location.protocol+"//"+window.location.host

		this.getServices = (scope, locale, id)->			
			http.post(this.url+"/"+locale+"/briefing/services/"+id+".json")
				.success(
					(data)->
						scope.services = data
						return
				)
			return

		this.getTypeProject = (scope, locale, id)->			
			http.post(this.url+"/"+locale+"/briefing/type_project/"+id+".json")
				.success(
					(data)->
						scope.data_type_project = data
						return
				)
			return

		return
]