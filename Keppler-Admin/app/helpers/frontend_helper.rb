module FrontendHelper
	def root_path?
		controller_name.eql?('frontend') and action_name.eql?('index')
	end

	def listing_blog?
		controller_name.eql?('blog') and (action_name.eql?('index') or action_name.eql?('filter'))
	end

	def show_blog?
		controller_name.eql?('blog') and action_name.eql?('show')
	end
end
