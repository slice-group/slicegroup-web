class NewsletterController < ApplicationController
  def create
  	gibbon = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)
  	gibbon.timeout = 10
  	@newsletter = Newsletter.new(newsletter_params)

  	respond_to do |format|
	  	if @newsletter.save
        begin
  		    gibbon.lists(Rails.application.secrets.mailchimp_list).members.create(body:{email_address: @newsletter.email, status: "subscribed"})
        rescue Gibbon::MailChimpError => e
          puts "Houston, we have a problem: #{e.message} - #{e.raw_body}"
        end
        format.html { redirect_to root_path, notice: "Gracias por suscribirse a nuestro boletín de noticias." }
	      format.json { render action: 'show', status: :created, location: @user }
	    else
	      format.html { redirect_to root_path(anchor: "footer"), alert: @newsletter.errors[:email].first }
	      format.json { render json: @newsletter.errors, status: :unprocessable_entity }
	    end
	  end
  end

  private

  def newsletter_params
    params.require(:newsletter).permit(:email)
  end
end
