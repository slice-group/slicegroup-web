class FrontendController < ApplicationController
  layout 'layouts/frontend/application'

  def index
    @projects = KepplerCatalogs::Catalog.where(section: ['web', 'design', 'app'], public: true).shuffle.first(9)
    @partners = KepplerCatalogs::Catalog.find_by_section('partners').attachments.where(public: true)
    @clients = KepplerCatalogs::Catalog.find_by_section('clients').attachments.where(public: true)
    #@message = KepplerContactUs::Message.new

    @data = {
      labels: ["#{t('frontend.area-charts.day_names.monday')}", "#{t('frontend.area-charts.day_names.tuesday')}", "#{t('frontend.area-charts.day_names.wednesday')}", "#{t('frontend.area-charts.day_names.thursday')}", "#{t('frontend.area-charts.day_names.friday')}", "#{t('frontend.area-charts.day_names.saturday')}", "#{t('frontend.area-charts.day_names.sunday')}"],
      datasets: [
        {
          label: "Cerveza",
          fillColor: "rgba(244, 67, 54, 0.90)",
          strokeColor: "rgba(244, 67, 54, 0.90)",
          pointColor: "rgba(244, 67, 54, 0.90)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [5, 1, 1, 1, 8, 13, 5]
        },
        {
          label: "Redbull",
          fillColor: "rgba(139, 195, 74, 0.90)",
          strokeColor: "rgba(139, 195, 74, 0.90)",
          pointColor: "rgba(139, 195, 74, 0.90)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [8, 4, 10, 3, 6, 4, 6]
        },
        {
          label: "Café",
          fillColor: "rgba(34, 152, 242, 0.90)",
          strokeColor: "rgba(34, 152, 242, 0.90)",
          pointColor: "rgba(34, 152, 242, 0.90)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [2, 9, 8, 6, 2, 2, 5]
        }
      ]
    }

    @options = {
      animation: true,
      datasetFill: true,
      scaleShowVerticalLines: false,
      bezierCurve: false,
      pointDot: false,
      scaleShowGridLines: false,
      responsive: true,
      datasetStroke: true,
      showTooltips: false,
      scaleShowLabels: false,
      scaleLineColor: "rgba(0,0,0,0)",
    }
  end

  def projects
    @projects = KepplerCatalogs::Catalog.where(section: ['web', 'design', 'app'], public: true).order('created_at DESC')
    @project = KepplerCatalogs::Catalog.find_by_permalink params[:catalog_permalink]
  end

  def portfolio_section
    @section = params[:section]
    if @section.eql?("all")
      @projects = KepplerCatalogs::Catalog.where(section: ['web', 'design', 'app'], public: true).order('created_at DESC')
      @project = KepplerCatalogs::Catalog.find_by_permalink params[:catalog_permalink]
    else
      @projects = KepplerCatalogs::Catalog.where(section: params[:section], public: true).order('created_at DESC')
    end
  end
end
