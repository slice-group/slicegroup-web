#Generado con Keppler.
class ServicesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_action :set_type_project

  # GET /services
  def index
    services = Service.searching(@query).where({type_project_id: params[:type_project_id]}).all
    @objects, @total = services.page(@current_page), services.size
    redirect_to type_project_services_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /services/1
  def show
  end

  # GET /services/new
  def new
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  def create
    @service = Service.new(service_params)
    @service.type_project_id = params[:type_project_id]

    if @service.save
      redirect_to type_project_services_path, notice: 'Service was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /services/1
  def update
    if @service.update(service_params)
      redirect_to type_project_services_path, notice: 'Service was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /services/1
  def destroy
    @service.destroy
    redirect_to services_url, notice: 'Service was successfully destroyed.'
  end

  def destroy_multiple
    Service.destroy redefine_ids(params[:multiple_ids])
    redirect_to type_project_services_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    def set_type_project
      @type_project = TypeProject.find_by_id(params[:type_project_id]).nil? ? redirect_to("/404") : TypeProject.find(params[:type_project_id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_params
      params.require(:service).permit(:name, :description, :type_project_id)
    end
end
