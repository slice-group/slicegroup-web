#Generado con Keppler.
class TypeProjectsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_type_project, only: [:show, :edit, :update, :destroy]

  # GET /type_projects
  def index
    type_projects = TypeProject.searching(@query).all
    @objects, @total = type_projects.page(@current_page), type_projects.size
    redirect_to type_projects_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /type_projects/1
  def show
  end

  # GET /type_projects/new
  def new
    @type_project = TypeProject.new
  end

  # GET /type_projects/1/edit
  def edit
  end

  # POST /type_projects
  def create
    @type_project = TypeProject.new(type_project_params)

    if @type_project.save
      redirect_to @type_project, notice: 'Type project was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /type_projects/1
  def update
    if @type_project.update(type_project_params)
      redirect_to @type_project, notice: 'Type project was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /type_projects/1
  def destroy
    @type_project.destroy
    redirect_to type_projects_url, notice: 'Type project was successfully destroyed.'
  end

  def destroy_multiple
    TypeProject.destroy redefine_ids(params[:multiple_ids])
    redirect_to type_projects_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_project
      @type_project = TypeProject.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def type_project_params
      params.require(:type_project).permit(:name, :description, :date_begin, :date_end, :budget, :machines, :persons, :departament)
    end
end
