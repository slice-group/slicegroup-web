#Generado con Keppler.
class PlannersController < ApplicationController
  before_filter :authenticate_user!, except: [:create, :new, :briefing, :planner_services_json, :planner_type_project_json]
  load_and_authorize_resource except: [:create, :new, :briefing, :planner_services_json, :planner_type_project_json]
  before_action :set_planner, only: [:show, :edit, :update, :destroy]
  layout :layout_by_resource

  # GET /planners
  def index
    planners = Planner.searching(@query).all
    @objects, @total = planners.page(@current_page), planners.size
    redirect_to planners_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /planners/1
  def show
    @planner.read = true
    @planner.save
  end

  # GET /planners/new
  def briefing
    @planner = Planner.new(type_project_id: TypeProject.first.id)
    @services = @planner.type_project.services
    @type_projects = TypeProject.all
    @budgets = if I18n.locale == :es
      [["1","Menos de Bs. 10.000"], ["2", "Bs. 10.000 - Bs. 100.000"], ["3", "Bs. 100.000 - Bs. 300.000"], ["4", "Mas de Bs. 300.000"]]
    else
      [["1","Under $1000"], ["2", "$1000 - $5000"], ["3", "$5000 - $15000"], ["4", "Over $15000"]]
    end
  end

  def planner_services_json
    services = TypeProject.find_by_id(params[:type_project_id]).services
    respond_to do |format|
      format.json  { render :json => services } # don't do msg.to_json
    end
  end

  def planner_type_project_json
    type_project = TypeProject.find_by_id(params[:type_project_id])
    respond_to do |format|
      format.json  { render :json => type_project } # don't do msg.to_json
    end
  end

  # GET /planners/1/edit
  def edit
  end

  # POST /planners
  def create
    @planner = Planner.new(planner_params)

    if @planner.save
      redirect_to root_path, notice: 'Ya hemos recibido su briefing, ¡muy pronto nos estaremos comunicando con usted!'
    else
      @services = @planner.type_project.services
      @type_projects = TypeProject.all
      @budgets = [["1","Menos de Bs. 10.000"], ["2", "Bs. 10.000 - Bs. 50.000"], ["3", "Bs. 50.000 - Bs. 100.000"], ["4", "Mas de Bs. 100.000"]]
      render :briefing
    end
  end

  # PATCH/PUT /planners/1
  def update
    if @planner.update(planner_params)
      redirect_to @planner, notice: 'Planner was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /planners/1
  def destroy
    @planner.destroy
    redirect_to planners_url, notice: 'Proyecto eliminado satisfactoriamente'
  end

  def destroy_multiple
    Planner.destroy redefine_ids(params[:multiple_ids])
    redirect_to planners_path(page: @current_page, search: @query), notice: "Proyectos eliminados satisfactoriamente"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_planner
      @planner = Planner.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def planner_params
      params.require(:planner).permit(:name, :email, :phone, :date_begin, :date_end, :machines, :persons, :description, :budget, :type_project_id, :service_ids => [])
    end

    def layout_by_resource
      if action_name == "briefing" or action_name == "create"
        'layouts/frontend/application'
      else
        "admin/application"
      end
    end
end
