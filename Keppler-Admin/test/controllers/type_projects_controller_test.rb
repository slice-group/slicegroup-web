require 'test_helper'

class TypeProjectsControllerTest < ActionController::TestCase
  setup do
    @type_project = type_projects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_projects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_project" do
    assert_difference('TypeProject.count') do
      post :create, type_project: { description: @type_project.description, name: @type_project.name }
    end

    assert_redirected_to type_project_path(assigns(:type_project))
  end

  test "should show type_project" do
    get :show, id: @type_project
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @type_project
    assert_response :success
  end

  test "should update type_project" do
    patch :update, id: @type_project, type_project: { description: @type_project.description, name: @type_project.name }
    assert_redirected_to type_project_path(assigns(:type_project))
  end

  test "should destroy type_project" do
    assert_difference('TypeProject.count', -1) do
      delete :destroy, id: @type_project
    end

    assert_redirected_to type_projects_path
  end
end
